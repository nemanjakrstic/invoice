<?php

use App\Role;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class RolesTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Role::truncate();

		Role::create(['name' => 'Administrator']);
		Role::create(['name' => 'Operator']);
		Role::create(['name' => 'Lager']);
	}

}
