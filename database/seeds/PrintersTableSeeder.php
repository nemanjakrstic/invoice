<?php

use App\Client;
use App\Printer;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PrintersTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Printer::truncate();

		$faker = Faker::create();

		foreach (range(1, 1000) as $i)
		{
			$manufacturer = $faker->company;

			$model = strtoupper($faker->word.' '.$faker->randomDigit);

			Printer::create([
				'manufacturer' => $manufacturer,
				'model' => $model,
				'full_name' => $manufacturer.' - '.$model,
			]);
		}

		$printers = Printer::all();

		foreach (Client::all() as $client)
		{
			$randomPrinters = $printers->random($faker->numberBetween(10, 20));

			foreach ($randomPrinters as $randomPrinter)
			{
				$client->printers()->attach($randomPrinter->id, ['quantity' => $faker->randomDigit]);
			}
		}
	}

}
