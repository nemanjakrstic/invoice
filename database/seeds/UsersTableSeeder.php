<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		User::truncate();

		User::create([
			'role_id' => 1,
			'name' => 'Master',
			'email' => 'master@invoiceapp.tk',
			'password' => 'master',
			'is_master' => 1,
		]);

		User::create([
			'role_id' => 2,
			'name' => 'John Doe',
			'email' => 'johndoe@invoiceapp.tk',
			'password' => 'johndoe',
		]);
	}

}
