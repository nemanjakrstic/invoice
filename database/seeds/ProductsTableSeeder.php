<?php

use App\Invoice;
use App\Product;
use App\InvoicedProduct;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ProductsTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Product::truncate();

		$faker = Faker::create();

		foreach (range(1, 1000) as $i)
		{
			Product::create([
				'product_number' => strtoupper(str_random(8)),
				'original_product_number' => strtoupper(str_random(8)),
				'name' => $faker->sentence(3),
				'description' => $faker->sentence(6),
				'details' => $faker->sentence(9),
				'purchase_price' => $faker->numberBetween(100, 1000),
			]);
		}

		$products = Product::all();

		foreach (Invoice::all() as $invoice)
		{
			$invoiced = $products->random($faker->numberBetween(3, 30));

			$i = 1;

			foreach ($invoiced as $product)
			{
				InvoicedProduct::create([
					'invoice_id' => $invoice->id,
					'position' => $i++,
					'product_number' => $product->product_number,
					'original_product_number' => $product->original_product_number,
					'name' => $product->name,
					'description' => $product->description,
					'details' => $product->details,
					'price' => $product->purchase_price,
					'quantity' => $faker->numberBetween(1, 50),
				]);
			}
		}
	}

}
