<?php

use App\Client;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ClientsTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Client::truncate();

		$faker = Faker::create();

		foreach (range(1, 100) as $i)
		{
			Client::create(['name' => $faker->name]);
		}
	}

}
