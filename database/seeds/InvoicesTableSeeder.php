<?php

use App\User;
use App\Client;
use App\Invoice;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class InvoicesTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Invoice::truncate();

		$faker = Faker::create();

		$client = Client::all();

		$users = User::all();

		foreach (range(1, 100) as $i)
		{
			$users->random()->invoices()->create([
				'client_id' => $client->random()->id,
				'code' => date('Y').str_pad($i, 4, '0', STR_PAD_LEFT),
				'price' => $faker->numberBetween(100, 10000),
				'status' => $faker->randomElement(['draft', 'offered', 'pending', 'accepted', 'paid']),
				'created_on' => $faker->date(),
			]);
		}
	}

}
