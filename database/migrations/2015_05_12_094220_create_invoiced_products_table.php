<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicedProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoiced_products', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('invoice_id')->index();
			$table->unsignedInteger('position');
			$table->string('product_number');
			$table->string('original_product_number');
			$table->string('name');
			$table->string('description');
			$table->string('details');
			$table->unsignedInteger('quantity');
			$table->decimal('price', 18, 2);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invoiced_products');
	}

}
