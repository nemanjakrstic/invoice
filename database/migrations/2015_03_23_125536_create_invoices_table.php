<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoices', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('creator_id');
			$table->unsignedInteger('client_id');
			$table->string('code')->nullable();
			$table->decimal('price', 18, 2)->nullable();
			$table->enum('status', ['draft', 'offered', 'pending', 'accepted', 'paid'])->nullable();
			$table->date('created_on')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invoices');
	}

}
