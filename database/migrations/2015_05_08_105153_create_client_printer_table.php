<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientPrinterTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('client_printer', function(Blueprint $table)
		{
			$table->unsignedInteger('client_id')->index();
			$table->unsignedInteger('printer_id')->index();
			$table->unsignedInteger('quantity');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('client_printer');
	}

}
