<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword, SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['role_id', 'name', 'email', 'password'];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['deleted_at'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	/**
	 * The relations to eager load on every query.
	 *
	 * @var array
	 */
	protected $with = ['role', 'permissions'];

	/**
	 * User can create many invoices.
	 *
	 * @return HasMany
	 */
	public function invoices()
	{
		return $this->hasMany('App\Invoice', 'creator_id');
	}

	/**
	 * User belongs to a role.
	 *
	 * @return BelongsTo
	 */
	public function role()
	{
		return $this->belongsTo('App\Role');
	}

	/**
	 * Hash user's password.
	 *
	 * @param string $input
	 */
	public function setPasswordAttribute($input)
	{
		$this->attributes['password'] = bcrypt($input);
	}

	/**
	 * User can have many permissions.
	 *
	 * @return HasMany
	 */
	public function permissions()
	{
		return $this->role->permissions();
	}

}
