<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'product_number',
		'original_product_number',
		'name',
		'description',
		'details',
		'purchase_price',
	];

}
