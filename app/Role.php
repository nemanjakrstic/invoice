<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'description'];

	public function permissions()
	{
		return $this->hasMany('App\Permission');
	}

	public function users()
	{
		return $this->hasMany('App\User');
	}

	public function hasPermission($route)
	{
		foreach ($this->permissions as $permission)
		{
			if (str_is($permission->route, $route))
			{
				return true;
			}
		}

		return false;
	}

}
