<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model {

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name'];

	/**
	 * Client has many printers.
	 *
	 * @return BelongsToMany
	 */
	public function printers()
	{
		return $this->belongsToMany('App\Printer')->withPivot('quantity');
	}

	/**
	 * Client has many invoices.
	 *
	 * @return HasMany
	 */
	public function invoices()
	{
		return $this->hasMany('App\Invoice');
	}

}
