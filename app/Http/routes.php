<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'DashboardController@index');

Route::get('home', 'DashboardController@index');

Route::controllers([
	'lang' => 'LangController',
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::resource('users', 'UsersController');
Route::resource('roles', 'RolesController');
Route::resource('clients', 'ClientsController');
Route::resource('clients.printers', 'ClientPrinterController');
Route::resource('clients.invoices', 'ClientInvoicesController', ['only' => 'store']);
Route::resource('permissions', 'PermissionsController');

Route::resource('invoices', 'InvoicesController');
Route::get('invoices/{invoices}/download', ['as' => 'invoices.download', 'uses' => 'InvoicesController@download']);

Route::group(['prefix' => 'api'], function()
{
	Route::resource('invoices', 'Api\InvoicesController');
	Route::resource('invoiced-products', 'Api\InvoicedProductsController', ['only' => ['store', 'update', 'destroy']]);
	Route::resource('printers', 'Api\PrintersController');
	Route::resource('products', 'Api\ProductsController');
});
