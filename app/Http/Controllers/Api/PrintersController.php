<?php namespace App\Http\Controllers\Api;

use App\Printer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrintersController extends Controller {

	/**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');

		$this->middleware('permission');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$printers = Printer::orderBy('full_name');

		// Filter printers by full_name.
		if ($fullName = $request->get('full_name'))
		{
			$printers = $printers->where('full_name', 'like', '%'.$fullName.'%');
		}

		$printers = $printers->limit(10)->get();

		return response()->json(['printers' => $printers]);
	}

}
