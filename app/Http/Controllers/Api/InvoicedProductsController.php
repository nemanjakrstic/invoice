<?php namespace App\Http\Controllers\Api;

use App\Product;
use App\Invoice;
use App\InvoicedProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InvoicedProductsController extends Controller {

	/**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('permission');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		// Find invoice.
		$invoice = Invoice::findOrFail($request->get('invoice_id'));

		// Find selected product.
		$found = Product::findOrFail($request->get('product_id'));

		// Create new invoiced product based on selected product.
		$product = new InvoicedProduct($found->toArray());
		$product->price = $found->purchase_price;
		$product->quantity = 1;
		$product->setPosition($invoice);

		// Attach product to invoice.
		$invoice->products()->save($product);

		// Increment invoice price.
		$invoice->updatePrice($product->price * $product->quantity)->save();

		// Return invoiced product in json format.
		return $product;
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		// Find invoiced product.
		$product = InvoicedProduct::findOrFail($id);

		// Update quantity and price only.
		$product->update($request->only(['quantity', 'price', 'status']));

		// Update invoice total price.
		$product->invoice->recalculatePrice()->save();

		// Return invoiced product in json format.
		return $product;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		// Find invoiced product.
		$product = InvoicedProduct::findOrFail($id);

		// Delete it from database.
		$product->delete();

		// Update invoiced product positions.
		$product->invoice->updateProductPositions();

		// Decrement invoice price.
		$product->invoice->updatePrice(-$product->price * $product->quantity)->save();

		// Return invoiced product in json format.
		return $product;
	}

}
