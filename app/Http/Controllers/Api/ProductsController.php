<?php namespace App\Http\Controllers\Api;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller {

	/**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$products = Product::orderBy('name');

		// Filter products by name.
		if ($name = $request->get('name'))
		{
			$products = $products->where('name', 'like', '%'.$name.'%');
		}

		$products = $products->limit(3)->get();

		return response()->json($products);
	}

}
