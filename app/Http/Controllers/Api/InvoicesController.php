<?php namespace App\Http\Controllers\Api;

use DB;
use Auth;
use Input;
use App\Invoice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateInvoiceRequest;

class InvoicesController extends Controller {

	/**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('permission');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$invoices = Invoice::orderBy('code');

		// Show all invoices if user is master.
		// If not, show only invoices that he created.
		if ( ! is_master())
		{
			$invoices = $invoices->where('creator_id', Auth::id());
		}

		// Filter invoices by code.
		if ($code = $request->get('code'))
		{
			$invoices = $invoices->where('code', 'like', $code.'%');
		}

		return $invoices->limit(10)->lists('code');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function show($id)
	{
		return Invoice::with('products')->findOrFail($id);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$invoice = Invoice::findOrFail($id);

		$invoice->update($request->all());

		$invoice->recalculatePrice()->save();

		return $invoice;
	}

}
