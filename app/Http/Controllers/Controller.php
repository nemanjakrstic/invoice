<?php namespace App\Http\Controllers;

use Input;
use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class Controller extends BaseController {

	use DispatchesCommands, ValidatesRequests;

	/**
	 * Get order by direction from query params.
	 *
	 * @return string
	 */
	protected function getOrderDir()
	{
		return Input::get('order_asc') == 1 ? 'asc' : 'desc';
	}

	/**
	 * Get order by field name from query params.
	 *
	 * @return string
	 */
	protected function getOrderBy($allowed = [])
	{
		$field = Input::get('order_by');

		if (in_array($field, $allowed))
		{
			return $field;
		}

		return null;
	}

}
