<?php namespace App\Http\Controllers;

use App\Client;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientPrinterController extends Controller {

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param int clientId Client ID
	 * @return Response
	 */
	public function store(Request $request, $clientId)
	{
		$client = Client::findOrFail($clientId);

		$client->printers()->detach($request->get('printer_id'));

		$client->printers()->attach($request->get('printer_id'), ['quantity' => $request->get('quantity')]);

		return redirect()->route('clients.show', $clientId);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $clientId
	 * @param  int  $printerId
	 * @return Response
	 */
	public function destroy($clientId, $printerId)
	{
		$client = Client::findOrFail($clientId);

		$client->printers()->detach($printerId);

		return redirect()->route('clients.show', $clientId);
	}

}
