<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class LangController extends Controller {

	/**
	 * Set application locale.
	 *
	 * @return Response
	 */
	public function getChange($locale)
	{
		if (in_array($locale, array_keys(trans('languages'))))
		{
			session()->set('locale', $locale);
		}

		return redirect()->back();
	}

}
