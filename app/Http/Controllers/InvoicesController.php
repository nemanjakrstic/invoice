<?php namespace App\Http\Controllers;

use PDF;
use Auth;
use Input;
use App\Invoice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InvoicesController extends Controller {

	/**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('permission');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		// Start query builder with order by.
		// Order by field defaults to 'payment_date'.
		$query = Invoice::start(
			$this->getOrderBy(['code', 'price', 'created_on']),
			$this->getOrderDir()
		);

		$query->with('client');

		// Show all invoices if user is master.
		// If not, show only invoices that he created.
		if ( ! is_master())
		{
			$query->where('creator_id', Auth::id());
		}

		// Filter invoices by code.
		if ($code = $request->get('code'))
		{
			$query->where('code', $code);
		}

		// Create paginated result.
		$invoices = $query->paginate();

		return view('invoices.index', compact('invoices'));
	}

	/**
	 * Show a single invoice.
	 *
	 * @param  $id Invoice ID
	 * @return Response
	 */
	public function show($id)
	{
		$invoice = Invoice::findOrFail($id);

		return view('invoices.show', compact('invoice'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function edit($id)
	{
		return view('invoices.edit', compact('id'));
	}

	/**
	 * Download invoice document.
	 *
	 * @param  $id Invoice ID
	 * @return Response
	 */
	public function download($id)
	{
		$invoice = Invoice::with('creator')->findOrFail($id);

		return PDF::loadView('pdf.invoice', compact('invoice'))->download(str_slug("invoice-{$invoice->code}").'.pdf');
	}

}
