<?php namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests\LoginUserRequest;
use App\Http\Requests\CreateUserRequest;
use Illuminate\Contracts\Auth\Registrar;

class AuthController extends Controller {

	/**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest', ['except' => 'getLogout']);
	}

	/**
	 * Show the application login form.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getLogin()
	{
		return view('auth.login');
	}

	/**
	 * Handle a login request to the application.
	 *
	 * @param  \App\Http\Requests\LoginUserRequest  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postLogin(LoginUserRequest $request)
	{
		if (Auth::attempt($request->only('email', 'password'), $request->has('remember')))
		{
			return redirect()->intended('/');
		}

		return redirect('auth/login')
					->withInput($request->only('email', 'remember'))
					->withErrors(['email' => 'These credentials do not match our records.']);
	}

	/**
	 * Log the user out of the application.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getLogout()
	{
		Auth::logout();

		return redirect('/');
	}

}
