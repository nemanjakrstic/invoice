<?php namespace App\Http\Controllers;

use App\Role;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PermissionsController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');

		$this->middleware('master');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$roles = Role::orderBy('name')->get();

		$resources = trans('permissions.resources');

		$methods = trans('permissions.methods');

		return view('permissions.index', compact('roles', 'resources', 'methods'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$role = Role::findOrFail($request->get('role_id'));

		$resource = $request->get('resource');

		$method = $request->get('method');

		foreach (trans("permissions.aliases.{$method}") as $alias)
		{
			$route = "{$resource}.{$alias}";

			if ($request->get('remove'))
			{
				$role->permissions()->where('route', $route)->delete();
			}
			else
			{
				$role->permissions()->create(['route' => $route]);
			}
		}

		return redirect()->route('permissions.index');
	}

}
