<?php namespace App\Http\Controllers;

use Auth;
use App\Client;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientInvoicesController extends Controller {

	/**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('permission');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param int $clientId Client ID
	 * @return Response
	 */
	public function store($clientId)
	{
		$client = Client::findOrFail($clientId);

		$invoice = Auth::user()->invoices()->create([
			'client_id' => $client->id,
			'status' => 'draft',
		]);

		$invoice->createCode()->save();

		return redirect()->route('invoices.edit', $invoice->id);
	}

}
