<?php namespace App;

use DB;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model {

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'client_id',
		'code',
		'price',
		'created_on',
		'status',
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer',
		'creator_id' => 'integer',
		'client_id' => 'integer',
		'price' => 'float',
	];

	/**
	 * Invoice is created by user.
	 *
	 * @return BelongsTo
	 */
	public function creator()
	{
		return $this->belongsTo('App\User', 'creator_id');
	}

	/**
	 * Created on date mutator.
	 *
	 * @param string $input
	 */
	public function setCreatedOnAttribute($input)
	{
		$this->attributes['created_on'] = Carbon::parse($input);
	}

	/**
	 * Created on date accessor.
	 *
	 * @param string $value
	 */
	public function getCreatedOnAttribute($value)
	{
		return $value ? Carbon::parse($value)->format('Y-m-d') : null;
	}

	/**
	 * Scope for starting new query with no order by.
	 */
	public function scopeStart($query, $orderBy = null, $orderDir = 'asc')
	{
		if (is_null($orderBy))
		{
			return $query;
		}

		return $query->orderBy($orderBy, $orderDir);
	}

	/**
	 * Invoice has many products.
	 *
	 * @return HasMany
	 */
	public function products()
	{
		return $this->hasMany('App\InvoicedProduct')->orderBy('position');
	}

	/**
	 * Update product positions.
	 *
	 * @return void
	 */
	public function updateProductPositions()
	{
		foreach ($this->products as $index => $product)
		{
			$product->setPosition($this, $index + 1)->save();
		}
	}

	/**
	 * Increment, decrement or calculate invoice price.
	 *
	 * @param int $price
	 * @return Invoice
	 */
	public function updatePrice($price)
	{
		$this->price += $price;

		return $this;
	}

	/**
	 * Recalculate invoice price.
	 *
	 * @return Invoice
	 */
	public function recalculatePrice()
	{
		$this->price = $this->products()->sum(DB::raw('price * quantity'));

		return $this;
	}

	/**
	 * Invoice belongs to a client.
	 *
	 * @return BelongsTo
	 */
	public function client()
	{
		return $this->belongsTo('App\Client');
	}

	/**
	 * Create invoice code.
	 *
	 * @return Invoice
	 */
	public function createCode()
	{
		$this->code = date('Y').str_pad($this->id, 4, '0', STR_PAD_LEFT);

		return $this;
	}

}
