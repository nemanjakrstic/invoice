<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoicedProduct extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'invoiced_products';

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer',
		'invoice_id' => 'integer',
		'position' => 'integer',
		'quantity' => 'integer',
		'price' => 'float',
	];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'invoice_id',
		'position',
		'product_number',
		'original_product_number',
		'name',
		'description',
		'details',
		'quantity',
		'price',
	];

	/**
	 * Invoiced product belongs to a invoice.
	 *
	 * @return BelongsTo
	 */
	public function invoice()
	{
		return $this->belongsTo('App\Invoice');
	}

	/**
	 * Update invoiced product position.
	 *
	 * @param int $position
	 */
	public function setPosition($invoice, $position = null)
	{
		if (is_null($position))
		{
			$this->position = $invoice->products()->count() + 1;
		}
		else
		{
			$this->position = $position;
		}

		return $this;
	}

}
