<?php namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class Authorize extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'authorize';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Give master privileges to user';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$email = $this->argument('email');

		if ($user = User::where('email', $email)->first())
		{
			$user->update(['is_master', 1]);

			$this->info("{$user->name} is now master.");
		}
		else
		{
			$this->error('User not found!');
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['email', InputArgument::REQUIRED, 'User\'s email.'],
		];
	}

}
