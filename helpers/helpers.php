<?php

function is_master()
{
	return Auth::user()->is_master == 1;
}

function is_allowed($route)
{
	if (is_master()) return true;

	return Auth::user()->role->hasPermission($route);
}

function sort_link($field, $name)
{
	// Get all input params.
	$inputs = Input::all();

	// Default class for all sort links.
	$class = 'sort-link';

	// Check if this is current sorting field.
	if (array_get($inputs, 'order_by') == $field)
	{
		$class .= $inputs['order_asc'] ? ' sort-link-asc' : ' sort-link-desc';
	}

	// Add order_by param to be given field.
	$inputs['order_by'] = $field;

	// Reverse order for the order_asc param.
	$inputs['order_asc'] = ! array_get($inputs, 'order_asc', 1);

	// Generate url with new params.
	$url = Request::url().'?'.http_build_query($inputs);

	// Generate html link with classes.
	return "<a href='{$url}' class='{$class}'>{$name}</a>";
}

function amount($amount)
{
	return number_format($amount, 2);
}

function asset_version($asset, $secure = null)
{
	$url = asset($asset, $secure);

	if (file_exists($path = public_path($asset)))
	{
		return $url.'?'.filemtime($path);
	}

	return $url;
}
