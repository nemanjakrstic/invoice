var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

// Compile Without Source Maps
elixir.config.sourcemaps = false;

elixir(function(mix) {

	// Styles
	mix.copy(
		'bower_components/bootstrap/dist/css/bootstrap.min.css',
		'resources/css/bootstrap.css'
	)
	.copy(
		'bower_components/selectize/dist/css/selectize.bootstrap3.css',
		'resources/css/selectize.css'
	)
	.copy(
		'bower_components/bootstrap/dist/fonts',
		'public/fonts'
	)
	.styles([
		'bootstrap.css',
		'selectize.css',
		'app.css'
	],
		'public/css/all.css'
	);

	// Scripts
	mix.copy(
		'bower_components/jquery/dist/jquery.js',
		'resources/js/jquery.js'
	)
	.copy(
		'bower_components/selectize/dist/js/standalone/selectize.js',
		'resources/js/selectize.js'
	)
	.copy(
		'bower_components/bootstrap/dist/js/bootstrap.js',
		'resources/js/bootstrap.js'
	)
	.copy(
		'bower_components/angular/angular.js',
		'resources/js/angular.js'
	)
	.scripts([
		'jquery.js',
		'selectize.js',
		'bootstrap.js',
		'angular.js',
		'app.js'
	],
		'public/js/all.js'
	);
});
