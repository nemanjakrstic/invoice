angular
	.module('invoice', [])
	.config(['$interpolateProvider', function($interpolateProvider) {
		$interpolateProvider.startSymbol('{~').endSymbol('~}');
	}])
	.controller('rootController', ['$scope', function($scope) {
		//
	}])
	.directive('autocomplete', [function() {
		return {
			restrict: 'E',
			template: [
				'<input name="{~ name ~}" ng-model="query" type="text" list="autocomplete" class="form-control">',
				'<datalist id="autocomplete">',
					'<option ng-repeat="item in items" value="{~ item ~}">',
				'</datalist>',
			].join(''),
			link: function(scope, element, attributes) {
				scope.remote = attributes.remote;
				scope.name = attributes.name;
				scope.query = attributes.value;
			},
			controller: ['$scope', '$timeout', '$http', function($scope, $timeout, $http) {
				$scope.query = '';
				$scope.items = [];
				var timeout, delay = 500;
				$scope.$watch('query', function() {
					$timeout.cancel(timeout);
					timeout = $timeout(function() {
						$http
							.get($scope.remote + '?' + $scope.name + '=' + $scope.query)
							.success(function(response) {
								$scope.items = response;
							});
					}, delay);
				});
			}]
		};
	}])
	.directive('selectize', ['$http', function($http) {
		return {
			restrict: 'A',
			link: function(scope, element, attributes) {
				element.selectize({
					valueField: attributes.valueField,
					labelField: attributes.labelField,
					searchField: attributes.labelField,
					create: false,
					render: {
						option: function(item, escape) {
							return [
								'<div>',
									escape(item[attributes.labelField]),
								'</div>'
							].join('')
						}
					},
					load: function(query, callback) {
						if (!query.length) return callback();
						$.ajax({
							url: attributes.remote + '?' + attributes.labelField + '=' + encodeURIComponent(query),
							type: 'GET',
							error: function() {
								callback();
							},
							success: function(reponse) {
								callback(reponse.printers);
							}
						});
					}
				});
			}
		};
	}])
	.controller('InvoiceProductsController', ['$scope', '$http', '$window', function($scope, $http, $window) {

		$scope.products = [];

		$scope.invoice = {};

		$scope.find = function() {
			$http
				.get('/api/products?name=' + $scope.query)
				.success(function(products) {
					$scope.products = products;
				});
		};

		$scope.get = function(id) {
			$http
				.get('/api/invoices/' + id)
				.success(function(invoice) {
					$scope.invoice = invoice;
				});
		};

		$scope.addProduct = function(product) {
			$http
				.post('/api/invoiced-products', { invoice_id: $scope.invoice.id, product_id: product.id })
				.success(function(product) {
					$scope.invoice.products.push(product);
				});
		};

		$scope.removeProduct = function(product, index) {
			$http
				.delete('/api/invoiced-products/' + product.id)
				.success(function(product) {
					$scope.invoice.products.splice(index, 1);
				});
		};

		$scope.updateProduct = function(product) {
			$http.put('/api/invoiced-products/' + product.id, product);
		};

		$scope.updateInvoice = function() {
			return $http.put('/api/invoices/' + $scope.invoice.id, $scope.invoice);
		};

		$scope.price = function() {
			var price = 0;
			angular.forEach($scope.invoice.products, function(product) {
				price += product.price * product.quantity;
			});
			return price;
		};

		$scope.save = function() {
			$scope.invoice.status = 'offered';
			$scope
				.updateInvoice()
				.success(function() {
					$window.location.href = '/invoices/' + $scope.invoice.id;
				});
		};
	}]);
