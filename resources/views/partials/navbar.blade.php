<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>

			<a class="navbar-brand" href="{{ url() }}">{{ env('APP_NAME') }}</a>
		</div>

		<div class="collapse navbar-collapse" id="navbar">
			@if (Auth::check())
				<ul class="nav navbar-nav">
					<li>
						<a href="{{ url() }}">@lang('partials.dashboard')</a>
					</li>

					@if (is_master())
						<li>
							<a href="{{ route('roles.index') }}">@lang('partials.roles')</a>
						</li>

						<li>
							<a href="{{ route('permissions.index') }}">@lang('partials.permissions')</a>
						</li>

						<li>
							<a href="{{ route('users.index') }}">@lang('partials.users')</a>
						</li>

						<li>
							<a href="{{ route('clients.index') }}">@lang('partials.clients')</a>
						</li>
					@endif

					@if (is_allowed('invoices.index'))
						<li>
							<a href="{{ route('invoices.index') }}">@lang('partials.invoices')</a>
						</li>
					@endif
				</ul>
			@endif

			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						@lang('partials.languages') <span class="caret"></span>
					</a>

					<ul class="dropdown-menu">
						@foreach (trans('languages') as $lang => $language)
							<li>
								<a href="{{ url('lang/change', $lang) }}">{{ $language }}</a>
							</li>
						@endforeach
					</ul>
				</li>

				@if (Auth::check())
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							{{ Auth::user()->name }} <span class="caret"></span>
						</a>

						<ul class="dropdown-menu">
							<li>
								<a href="{{ url('auth/logout') }}">@lang('partials.logout')</a>
							</li>
						</ul>
					</li>
				@endif
			</ul>
		</div>
	</div>
</nav>
