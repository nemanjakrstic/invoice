@extends('app')

@section('content')
	<div class="container">
		@foreach ($methods as $methodKey => $methodName)
			<div class="panel panel-default">
				<div class="panel-heading">{{ $methodName }} &raquo; @lang('permissions.permissions')</div>

				<table class="table table-bordered">
					<thead>
						<tr>
							<td class="text-center text-muted">@lang('permissions.resource_role')</td>

							@foreach ($roles as $role)
								<td class="text-center" title="{{ $role->description }}">{{ $role->name }}</td>
							@endforeach
						</tr>
					</thead>

					<tbody>
						@foreach ($resources as $resourceKey => $resourceName)
							<tr>
								<td class="text-center">{{ $resourceName }}</td>

								@foreach ($roles as $role)
									<td class="text-center">
										{!! Form::open(['url' => route('permissions.store'), 'method' => 'post']) !!}
											<input type="hidden" name="role_id" value="{{ $role->id }}">
											<input type="hidden" name="resource" value="{{ $resourceKey }}">
											<input type="hidden" name="method" value="{{ $methodKey }}">

											@if ($role->hasPermission($resourceKey.'.'.$methodKey))
												<button type="submit" class="btn btn-success btn-xs" name="remove" value="1">
													<i class="fa fa-check"></i>
												</button>
											@else
												<button type="submit" class="btn btn-danger btn-xs" name="remove" value="0">
													<i class="fa fa-remove"></i>
												</button>
											@endif
										{!! Form::close() !!}
									</td>
								@endforeach
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		@endforeach
	</div>
@stop
