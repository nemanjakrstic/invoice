@extends('app')

@section('content')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">@lang('dashboard.dashboard')</div>

			<div class="panel-body">@lang('dashboard.you_are_logged_in')</div>
		</div>
	</div>
@endsection
