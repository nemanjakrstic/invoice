@extends('layouts.pdf')

@section('title', 'Invoice')

@section('content')
	<img width="100" src="img/ubuntu-logo.png" />

	<h2>
		Invoice <strong>{{ $invoice->code }}</strong>
	</h2>

	<h4>Products</h4>

	<table class="table">
		<tr>
			<th>Product Number</th>
			<th>Quantity</th>
			<th>Price</th>
			<th>Total</th>
		</tr>

		@foreach ($invoice->products as $product)
			<tr>
				<td>{{ $product->product_number }}</td>
				<td>{{ $product->quantity }}</td>
				<td>{{ amount($product->price) }}</td>
				<td>{{ amount($product->quantity * $product->price) }}</td>
			</tr>
		@endforeach
	</table>
@stop
