@extends('app')

@section('content')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">@lang('roles.roles')</div>

			<ul class="list-group">
				@foreach ($roles as $role)
					<li class="list-group-item">
						<span class="pull-right">
							<a href="{{ route('roles.edit', $role->id) }}" class="btn btn-warning">@lang('roles.edit')</a>
						</span>

						<h4 class="list-group-item-heading">{{ $role->name }}</h4>

						<p class="list-group-item-text text-muted">
							{{ empty($role->description) ? 'No description' : $role->description }}
						</p>
					</li>
				@endforeach
			</ul>

			<div class="panel-footer">
				<a href="{{ route('roles.create') }}" class="btn btn-primary">@lang('roles.create')</a>
			</div>
		</div>

		{!! $roles->render() !!}
	</div>
@stop
