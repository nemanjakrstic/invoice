@extends('layouts.form')

@section('form.content')
	<div class="panel panel-default">
		<div class="panel-heading">@lang('roles.edit_role')</div>

		<div class="panel-body">
			@include('partials.errors')

			{!! Form::model($role, ['url' => route('roles.update', $role), 'method' => 'put', 'class' => 'form-horizontal']) !!}
				@include('roles.form')

				<div class="form-group">
					<div class="col-md-offset-3 col-md-6">
						<button type="submit" class="btn btn-success">@lang('roles.update')</button>
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">@lang('roles.delete_role')</div>

		<div class="panel-body">
			{!! Form::open(['url' => route('roles.destroy', $role->id), 'method' => 'delete', 'class' => 'form-horizontal']) !!}
				<div class="form-group">
					<label class="col-md-3 control-label">@lang('roles.delete_role')</label>

					<div class="col-md-6">
						<button type="submit" class="btn btn-danger">@lang('roles.delete')</button>
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@endsection
