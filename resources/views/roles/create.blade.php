@extends('layouts.form')

@section('form.content')
	<div class="panel panel-default">
		<div class="panel-heading">@lang('roles.create_role')</div>

		<div class="panel-body">
			@include('partials.errors')

			{!! Form::open(['url' => route('roles.store'), 'method' => 'post', 'class' => 'form-horizontal']) !!}
				@include('roles.form')

				<div class="form-group">
					<div class="col-md-offset-3 col-md-6">
						<button type="submit" class="btn btn-success">@lang('roles.create')</button>
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@endsection
