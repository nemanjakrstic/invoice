<div class="form-group">
	<label class="col-md-3 control-label">@lang('roles.name')</label>

	<div class="col-md-6">
		{!! Form::text('name', null, ['class' => 'form-control']) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-md-3 control-label">@lang('roles.description')</label>

	<div class="col-md-6">
		{!! Form::text('description', null, ['class' => 'form-control']) !!}
	</div>
</div>
