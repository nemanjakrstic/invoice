<div class="form-group">
	<label class="col-md-3 control-label">@lang('users.name')</label>

	<div class="col-md-6">
		{!! Form::text('name', null, ['class' => 'form-control']) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-md-3 control-label">@lang('users.email')</label>

	<div class="col-md-6">
		{!! Form::text('email', null, ['class' => 'form-control']) !!}
	</div>
</div>

@unless (isset($user))
	<div class="form-group">
		<label class="col-md-3 control-label">@lang('users.password')</label>

		<div class="col-md-6">
			{!! Form::password('password', ['class' => 'form-control']) !!}
		</div>
	</div>
@endunless

<div class="form-group">
	<label class="col-md-3 control-label">@lang('users.role')</label>

	<div class="col-md-6">
		{!! Form::select('role_id', $roles, null, ['class' => 'form-control']) !!}
	</div>
</div>
