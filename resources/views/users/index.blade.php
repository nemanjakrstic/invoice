@extends('app')

@section('content')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">@lang('users.users')</div>

			<ul class="list-group">
				@foreach ($users as $user)
					<li class="list-group-item">
						<span class="pull-right">
							<a href="{{ route('users.edit', $user->id) }}" class="btn btn-warning">@lang('users.edit')</a>
						</span>

						<h4 class="list-group-item-heading">{{ $user->name }}</h4>

						<p class="list-group-item-text">
							<span class="text-muted">{{ $user->role->name }}</span>
							<br>
							<a href="mailto:{{ $user->email }}">{{ $user->email }}</a>
						</p>
					</li>
				@endforeach
			</ul>

			<div class="panel-footer">
				<a href="{{ route('users.create') }}" class="btn btn-primary">@lang('users.create')</a>
			</div>
		</div>

		<div class="text-center">{{ $users->render() }}</div>
	</div>
@stop
