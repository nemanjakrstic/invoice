@extends('layouts.form')

@section('form.content')
	<div class="panel panel-default">
		<div class="panel-heading">@lang('users.edit_user')</div>

		<div class="panel-body">
			@include('partials.errors')

			{!! Form::model($user, ['url' => route('users.update', $user->id), 'method' => 'put', 'class' => 'form-horizontal']) !!}
				@include('users.form')

				<div class="form-group">
					<div class="col-md-offset-3 col-md-6">
						<button type="submit" class="btn btn-success">@lang('users.update')</button>
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>

	@unless ($user->is_master)
		<div class="panel panel-default">
			<div class="panel-heading">@lang('users.delete')</div>

			<div class="panel-body">
				{!! Form::open(['url' => route('users.destroy', $user->id), 'method' => 'delete', 'class' => 'form-horizontal']) !!}
					<div class="form-group">
						<label class="col-md-3 control-label">@lang('users.delete_user')</label>

						<div class="col-md-6">
							<button type="submit" class="btn btn-danger">@lang('users.delete')</button>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	@endunless
@endsection
