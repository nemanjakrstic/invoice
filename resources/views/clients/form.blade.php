<div class="form-group">
	<label class="col-md-3 control-label">@lang('clients.name')</label>

	<div class="col-md-6">
		{!! Form::text('name', null, ['class' => 'form-control']) !!}
	</div>
</div>
