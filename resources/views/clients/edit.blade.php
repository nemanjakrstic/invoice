@extends('layouts.form')

@section('form.content')
	<div class="panel panel-default">
		<div class="panel-heading">@lang('clients.edit_client')</div>

		<div class="panel-body">
			@include('partials.errors')

			{!! Form::model($client, ['url' => route('clients.update', $client), 'method' => 'put', 'class' => 'form-horizontal']) !!}
				@include('clients.form')

				<div class="form-group">
					<div class="col-md-offset-3 col-md-6">
						<button type="submit" class="btn btn-success">@lang('clients.update')</button>
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">@lang('clients.delete_client')</div>

		<div class="panel-body">
			{!! Form::open(['url' => route('clients.destroy', $client->id), 'method' => 'delete', 'class' => 'form-horizontal']) !!}
				<div class="form-group">
					<label class="col-md-3 control-label">@lang('clients.delete_client')</label>

					<div class="col-md-6">
						<button type="submit" class="btn btn-danger">@lang('clients.delete')</button>
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@endsection
