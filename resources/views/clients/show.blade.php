@extends('app')

@section('modal')
	<div class="modal fade" id="addPrinterModal">
		<div class="modal-dialog">
			<div class="modal-content">
				{!! Form::open(['route' => ['clients.printers.store', $client->id], 'method' => 'post']) !!}
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Add new printer</h4>
					</div>

					<div class="modal-body">
						<div class="form-group">
							<label>Printer name</label>
							<select selectize name="printer_id" value-field="id" label-field="full_name" remote="{{ route('api.printers.index') }}"></select>
						</div>

						<div class="form-group">
							<label>Quantity</label>
							<input type="number" name="quantity" value="1" min="1" class="form-control">
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Add</button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@stop

@section('content')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">@lang('clients.clients')</div>

			<div class="panel-body">
				<h4>{{ $client->name }}</h4>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">Invoices</div>

				<div class="table-responsive">
					<table class="table table-hover">
						<thead>
							<tr>
								<th>@lang('invoices.code')</th>
								<th>Price</th>
								<th>Invoice Date</th>
								<th>@lang('invoices.created_by')</th>
								<th>Status</th>
								<th></th>
							</tr>
						</thead>

						<tbody>
							@forelse ($client->invoices as $invoice)
								<tr>
									<td>
										<a href="{{ route('invoices.show', $invoice->id) }}">{{ $invoice->code }}</a>
									</td>

									<td>{{ amount($invoice->price) }}</td>
									<td>{{ $invoice->created_on }}</td>
									<td>{{ $invoice->creator->name }}</td>
									<td>{{ ucfirst($invoice->status) }}</td>
									<td class="text-right">
										@if ($invoice->status == 'draft')
											<a class="btn btn-xs btn-warning" href="{{ route('invoices.edit', $invoice->id) }}">Edit</a>
										@endif

										<a class="btn btn-xs btn-success" href="{{ route('invoices.download', $invoice->id) }}">Download</a>
									</td>
								</tr>
							@empty
								<tr>
									<td class="text-muted" colspan="100%">@lang('invoices.no_invoices')</td>
								</tr>
							@endforelse
						</tbody>
					</table>
				</div>

				<div class="panel-footer">
					{!! Form::open(['route' => ['clients.invoices.store', $client->id]]) !!}
						{!! Form::submit('New Invoice', ['class' => 'btn btn-primary']) !!}
					{!! Form::close() !!}
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">@lang('clients.printers')</div>

				<ul class="list-group">
					@foreach ($client->printers as $printer)
						<li class="list-group-item">
							<span class="badge">{{ $printer->pivot->quantity }}</span>
							<h4 class="list-group-item-heading">{{ $printer->full_name }}</h4>

							<p class="list-group-item-text">
								{!! Form::open(['route' => ['clients.printers.destroy', $client->id, $printer->id], 'method' => 'delete']) !!}
									<button type="submit" class="btn btn-danger btn-xs">@lang('clients.remove')</button>
								{!! Form::close() !!}
							</p>
						</li>
					@endforeach
				</ul>

				<div class="panel-footer">
					<button class="btn btn-primary" data-toggle="modal" data-target="#addPrinterModal">@lang('clients.add')</button>
				</div>
			</div>
		</div>
	</div>
@stop
