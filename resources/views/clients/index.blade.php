@extends('app')

@section('content')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">@lang('clients.clients')</div>

			<div class="list-group">
				@foreach ($clients as $client)
					<a class="list-group-item" href="{{ route('clients.show', $client->id) }}">
						<h4>{{ $client->name }}</h4>
					</a>
				@endforeach
			</div>

			<div class="panel-footer">
				<a href="{{ route('clients.create') }}" class="btn btn-primary">@lang('clients.create')</a>
			</div>
		</div>

		{!! $clients->render() !!}
	</div>
@stop
