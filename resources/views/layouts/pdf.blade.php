<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>@yield('title')</title>

		<style type="text/css">
			@page {
				margin: 2cm;
			}

			body {
				font-family: sans-serif;
				margin: 0.5cm 0;
				text-align: justify;
			}

			.header,
			.footer {
				position: fixed;
				left: 0;
				right: 0;
				color: #aaa;
				font-size: 0.9em;
			}

			.header {
				top: 0;
				border-bottom: 0.1pt solid #aaa;
				padding-bottom: 4pt;
			}

			.footer {
				bottom: 0;
				border-top: 0.1pt solid #aaa;
				padding-top: 4pt;
			}

			.header table,
			.footer table {
				width: 100%;
				border-collapse: collapse;
				border: none;
			}

			.header td,
			.footer td {
				padding: 0;
				width: 50%;
			}

			.page-number {
				text-align: center;
			}

			.page-number:before {
				content: "Page " counter(page);
			}

			hr {
				page-break-after: always;
				border: 0;
			}

			.table {
				width: 100%;
				border: solid thin;
				border-collapse: collapse;
			}

			.table th,
			.table td {
				border: solid thin;
				padding: 1pt 4pt;
			}

			.table td {
				white-space: nowrap;
				vertical-align: top;
			}

			.table th {
				font-weight: bold;
				padding: 0.2em;
				vertical-align: middle;
				text-align: left;
			}
		</style>
	</head>

	<body>
		<div class="header">
			<table>
				<tr>
					<td>@yield('title')</td>
					<td style="text-align: right;">@yield('author')</td>
				</tr>
			</table>
		</div>

		<div class="footer">
			<div class="page-number"></div>
		</div>

		@yield('content')
	</body>
</html>
