@extends('app')

@section('content')
	<div class="container">
		<div class="table-responsive">
			@yield('table.content')
		</div>
	</div>
@stop
