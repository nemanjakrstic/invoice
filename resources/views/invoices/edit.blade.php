@extends('app')

@section('content')
	<div class="container-fluid" ng-controller="InvoiceProductsController" ng-init="get({{ $id }})">
		<div class="row">
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">Invoice Details</div>

					<div class="panel-body form-horizontal">
						<div class="form-group">
							<label class="col-md-3 control-label">Code</label>

							<div class="col-md-6">
								<input class="form-control"
									type="text"
									ng-model="invoice.code"
									ng-blur="updateInvoice()">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label">Price</label>

							<div class="col-md-6">
								<input class="form-control" type="text" value="{~ price() | number : 2 ~}" disabled>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label">Created On</label>

							<div class="col-md-6">
								<input class="form-control"
									type="text"
									format="Y-m-d"
									ng-model="invoice.created_on"
									ng-blur="updateInvoice()">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-3">
								<button class="btn btn-success" ng-click="save()" ng-disabled="invoice.products == 0 || working">
									<i class="fa fa-spinner fa-spin" ng-show="working"></i> Save
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-heading">Find Products</div>

					<div class="panel-body">
						<div class="form-group">
							<input placeholder="Search product by name"
								type="text"
								class="form-control"
								ng-model-options="{ debounce: 500 }"
								ng-model="query"
								ng-change="find()">
						</div>
					</div>

					<div class="list-group" ng-if="products.length > 0">
						<a href="#" class="list-group-item cursor-pointer" ng-repeat="product in products" ng-click="addProduct(product)">
							<h4 class="list-group-item-heading">{~ product.name ~}</h4>
							<p class="list-group-item-text">Description: {~ product.description ~}</p>
							<p class="list-group-item-text">Details: {~ product.details ~}</p>
							<p class="list-group-item-text">Price: {~ product.purchase_price ~}</p>
						</a>
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">Products</div>

					<div class="table-responsive">
						<table class="table table-hover table-valign">
							<thead>
								<tr>
									<th>Position</th>
									<th>Name</th>
									<th>Description</th>
									<th>Quantity</th>
									<th>Price</th>
									<th>Total</th>
									<th></th>
								</tr>
							</thead>

							<tbody>
								<tr ng-repeat="product in invoice.products track by $index">
									<td>{~ $index + 1 ~}</td>
									<td>{~ product.name ~}</td>
									<td>{~ product.description ~}</td>

									<td>
										<input class="form-control"
											type="number"
											min="1"
											ng-model="product.quantity"
											ng-blur="updateProduct(product)">
									</td>

									<td>
										<input class="form-control"
											type="number"
											min="1"
											step="0.01"
											ng-model="product.price"
											ng-blur="updateProduct(product)">
									</td>

									<td>{~ product.price * product.quantity | number : 2 ~}</td>

									<td>
										<span ng-click="removeProduct(product, $index)" class="cursor-pointer text-danger">Remove</span>
									</td>
								</tr>

								<tr ng-if="invoice.products.length == 0">
									<td colspan="100%">No products selected</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="panel-footer">Total {~ price() | number : 2 ~}</div>
				</div>
			</div>
		</div>
	</div>
@endsection
