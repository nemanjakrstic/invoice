@extends('layouts.form')

@section('form.content')
	<div class="panel panel-default">
		<div class="panel-heading">@lang('invoices.invoices') &raquo; {{ $invoice->code }}</div>

		<ul class="list-group">
			<li class="list-group-item">
				<h4 class="list-group-item-heading">{{ $invoice->code }}</h4>
				<p class="list-group-item-text text-muted">@lang('invoices.code')</p>
			</li>

			<li class="list-group-item">
				<h4 class="list-group-item-heading">{{ $invoice->price }}</h4>
				<p class="list-group-item-text text-muted">@lang('invoices.amount')</p>
			</li>

			<li class="list-group-item">
				<h4 class="list-group-item-heading">{{ $invoice->created_on }}</h4>
				<p class="list-group-item-text text-muted">@lang('invoices.issue_date')</p>
			</li>

			<li class="list-group-item">
				<h4 class="list-group-item-heading">{{ $invoice->client->name }}</h4>
				<p class="list-group-item-text text-muted">Client</p>
			</li>
		</ul>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">Products</div>

		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Product Number</th>
						<th>Quantity</th>
						<th class="text-right">Price</th>
						<th class="text-right">Total</th>
					</tr>
				</thead>

				<tbody>
					@foreach ($invoice->products as $product)
						<tr>
							<td>{{ $product->product_number }}</td>
							<td>{{ $product->quantity }}</td>
							<td class="text-right">{{ amount($product->price) }}</td>
							<td class="text-right">{{ amount($product->price * $product->quantity) }}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection
