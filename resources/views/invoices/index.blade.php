@extends('app')

@section('content')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">@lang('invoices.invoices')</div>

			<!-- Filter form start -->
			<div class="panel-body">
				{!! Form::open(['url' => route('invoices.index'), 'method' => 'get', 'class' => 'form-horizontal']) !!}
					<div class="form-group">
						<label class="col-md-3 control-label">@lang('invoices.code')</label>

						<div class="col-md-6">
							<autocomplete name="code" value="{{ Input::get('code') }}" remote="{{ route('api.invoices.index') }}">
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-offset-3 col-md-6">
							<button type="submit" class="btn btn-default">@lang('invoices.search')</button>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<!-- Filter form end -->

			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>{!! sort_link('code', trans('invoices.code')) !!}</th>
							<th>{!! sort_link('price', 'Price') !!}</th>
							<th>{!! sort_link('created_on', 'Invoice Date') !!}</th>
							<th>@lang('invoices.created_by')</th>
							<th>Status</th>
							<th></th>
						</tr>
					</thead>

					<tbody>
						@forelse ($invoices as $invoice)
							<tr>
								<td>
									<a href="{{ route('invoices.show', $invoice->id) }}">{{ $invoice->code }}</a>
								</td>

								<td>{{ amount($invoice->price) }}</td>
								<td>{{ $invoice->created_on }}</td>
								<td>{{ $invoice->creator->name }}</td>
								<td>{{ ucfirst($invoice->status) }}</td>
								<td class="text-right">
									<a class="btn btn-xs btn-success" href="{{ route('invoices.download', $invoice->id) }}">Download</a>
								</td>
							</tr>
						@empty
							<tr>
								<td class="text-muted" colspan="100%">@lang('invoices.no_invoices')</td>
							</tr>
						@endforelse
					</tbody>
				</table>
			</div>
		</div>

		<!-- Render pagination links -->
		<div class="text-center">{!! $invoices->appends(Input::all())->render() !!}</div>
	</div>
@stop
