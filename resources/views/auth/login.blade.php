@extends('layouts.form')

@section('form.content')
	<div class="panel panel-default">
		<div class="panel-heading">@lang('auth.login')</div>

		<div class="panel-body">
			@include('partials.errors')

			{!! Form::open(['url' => 'auth/login', 'method' => 'post', 'class' => 'form-horizontal']) !!}
				<div class="form-group">
					<label class="col-md-3 control-label">@lang('auth.email')</label>

					<div class="col-md-6">
						<input type="email" class="form-control" name="email" value="{{ old('email') }}">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label">@lang('auth.password')</label>

					<div class="col-md-6">
						<input type="password" class="form-control" name="password">
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-6 col-md-offset-3">
						<div class="checkbox">
							<label>
								<input type="checkbox" name="remember"> @lang('auth.remember_me')
							</label>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-6 col-md-offset-3">
						<button type="submit" class="btn btn-primary">@lang('auth.login')</button>

						<a class="btn btn-link" href="{{ url('password/email') }}">@lang('auth.forgot_your_password')</a>
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@endsection
