@extends('layouts.form')

@section('form.content')
	<div class="panel panel-default">
		<div class="panel-heading">@lang('auth.reset_password')</div>

		<div class="panel-body">
			@if (session('status'))
				<div class="alert alert-success">
					{{ session('status') }}
				</div>
			@endif

			@include('partials.errors')

			{!! Form::open(['url' => 'password/email', 'method' => 'post', 'class' => 'form-horizontal']) !!}
				<div class="form-group">
					<label class="col-md-3 control-label">@lang('auth.email')</label>

					<div class="col-md-6">
						<input type="email" class="form-control" name="email" value="{{ old('email') }}">
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-6 col-md-offset-3">
						<button type="submit" class="btn btn-primary">@lang('auth.send_password_reset_link')</button>
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@endsection
