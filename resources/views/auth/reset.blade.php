@extends('layouts.form')

@section('form.content')
	<div class="panel panel-default">
		<div class="panel-heading">@lang('auth.reset_password')</div>

		<div class="panel-body">
			@include('partials.errors')

			{!! Form::open(['url' => 'password/reset', 'method' => 'post', 'class' => 'form-horizontal']) !!}
				<input type="hidden" name="token" value="{{ $token }}">

				<div class="form-group">
					<label class="col-md-3 control-label">@lang('auth.email')</label>

					<div class="col-md-6">
						<input type="email" class="form-control" name="email" value="{{ old('email') }}">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label">@lang('auth.password')</label>

					<div class="col-md-6">
						<input type="password" class="form-control" name="password">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label">@lang('auth.confirm_password')</label>

					<div class="col-md-6">
						<input type="password" class="form-control" name="password_confirmation">
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-6 col-md-offset-3">
						<button type="submit" class="btn btn-primary">@lang('auth.reset_password')</button>
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@endsection
