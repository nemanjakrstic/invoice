<?php

return [

	'methods' => [

		'show' => 'Show',

		'create' => 'Create',

		'edit' => 'Edit',

		'destroy' => 'Delete',

	],

	'aliases' => [

		'show' => ['show', 'index'],

		'create' => ['create', 'store'],

		'edit' => ['edit', 'update'],

		'destroy' => ['destroy'],

	],

	'resources' => [

		'invoices' => 'Invoices',

		'problems' => 'Problems',

	],

	"permissions" => "Permissions",

	"resource_role" => "Resource / Role",

];
