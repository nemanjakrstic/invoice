<?php

return [

	"create_role" => "Create Role",

	"create" => "Create",

	"edit_role" => "Edit Role",

	"edit" => "Edit",

	"update" => "Update",

	"delete_role" => "Delete Role",

	"delete" => "Delete",

	"name" => "Name",

	"description" => "Description",

	"roles" => "Roles",

];
