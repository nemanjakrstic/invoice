<?php

return [

	"dashboard" => "Dashboard",

	"roles" => "Roles",

	"permissions" => "Permissions",

	"users" => "Users",

	"clients" => "Clients",

	"invoices" => "Invoices",

	"languages" => "Languages",

	"login" => "Login",

	"logout" => "Logout",

	"whoops" => "Whoops!",

	"problems_with_input" => "There were some problems with your input.",

];
