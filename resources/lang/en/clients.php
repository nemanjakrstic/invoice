<?php

return [

	"create_client" => "Create Client",

	"create" => "Create",

	"edit_client" => "Edit Client",

	"edit" => "Edit",

	"update" => "Update",

	"delete_client" => "Delete Client",

	"delete" => "Delete",

	"name" => "Name",

	"clients" => "Clients",

	"printers" => "Printers",

	"add" => "Add",

	"remove" => "Remove",

];
