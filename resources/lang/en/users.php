<?php

return [

	"create_user" => "Create User",

	"create" => "Create",

	"edit_user" => "Edit User",

	"edit" => "Edit",

	"update" => "Update",

	"delete" => "Delete",

	"delete_user" => "Delete User",

	"name" => "Name",

	"email" => "Email",

	"password" => "Password",

	"role" => "Role",

	"users" => "Users",

];
