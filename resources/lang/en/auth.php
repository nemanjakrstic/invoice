<?php

return [

	"login" => "Login",

	"email" => "E-Mail Address",

	"password" => "Password",

	"remember_me" => "Remember Me",

	"forgot_your_password" => "Forgot Your Password?",

	"reset_password" => "Reset Password",

	"send_password_reset_link" => "Send Password Reset Link",

	"confirm_password" => "Confirm Password",

];
