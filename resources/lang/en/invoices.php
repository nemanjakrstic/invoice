<?php

return [

	"create_invoice" => "Create Invoice",

	"code" => "Code",

	"amount" => "Amount",

	"issue_date" => "Issue Date",

	"payment_date" => "Payment Date",

	"save" => "Save",

	"invoices" => "Invoices",

	"created_by" => "Created By",

	"create" => "Create",

	"no_invoices" => "No invoices",

	"search" => "Search",

];
