<?php

return [

	"create_invoice" => "Napravi fakturu",

	"code" => "Šifra",

	"amount" => "Iznos",

	"issue_date" => "Datum izdavanja",

	"payment_date" => "Datum plaćanja",

	"save" => "Snimi",

	"invoices" => "Fakture",

	"created_by" => "Napravio",

	"create" => "Napravi",

	"no_invoices" => "Nema faktura",

	"search" => "Pretraga",

];
