<?php

return [

	"create_role" => "Napravi ulogu",

	"create" => "Napravi",

	"edit_role" => "Izmeni ulogu",

	"edit" => "Izmeni",

	"update" => "Izmeni",

	"delete_role" => "Obriši ulogu",

	"delete" => "Obriši",

	"name" => "Naziv",

	"description" => "Opis",

	"roles" => "Uloge",

];
