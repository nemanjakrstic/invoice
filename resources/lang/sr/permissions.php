<?php

return [

	'methods' => [

		'show' => 'Prikaz',

		'create' => 'Unos',

		'edit' => 'Izmena',

		'destroy' => 'Brisanje',

	],

	'aliases' => [

		'show' => ['show', 'index'],

		'create' => ['create', 'store'],

		'edit' => ['edit', 'update'],

		'destroy' => ['destroy'],

	],

	'resources' => [

		'invoices' => 'Fakture',

		'problems' => 'Problemi',

	],

	"permissions" => "Dozvole",

	"resource_role" => "Resurs / Uloga",

];
