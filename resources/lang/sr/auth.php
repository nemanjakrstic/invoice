<?php

return [

	"login" => "Prijava",

	"email" => "E-Mail Adresa",

	"password" => "Lozinka",

	"remember_me" => "Zapamti me",

	"forgot_your_password" => "Zaboravili ste lozinku?",

	"reset_password" => "Resetujte lozinku",

	"send_password_reset_link" => "Pošalji link za reset lozinke",

	"confirm_password" => "Potvrdite lozinku",

];
