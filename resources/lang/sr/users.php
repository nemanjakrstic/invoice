<?php

return [

	"create_user" => "Napravi korisnika",

	"create" => "Napravi",

	"edit_user" => "Izmeni korisnika",

	"edit" => "Izmeni",

	"update" => "Izmeni",

	"delete" => "Obriši",

	"delete_user" => "Obriši korisnika",

	"name" => "Ime",

	"email" => "Email",

	"password" => "Lozinka",

	"role" => "Uloga",

	"users" => "Korisnici",

];
