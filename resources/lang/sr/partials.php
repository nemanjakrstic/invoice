<?php

return [

	"dashboard" => "Komandna tabla",

	"roles" => "Uloge",

	"permissions" => "Dozvole",

	"users" => "Korisnici",

	"invoices" => "Fakture",

	"languages" => "Jezici",

	"login" => "Prijava",

	"logout" => "Odjava",

	"whoops" => "Ups!",

	"problems_with_input" => "Našli smo grešku u vašim podacima.",

];
